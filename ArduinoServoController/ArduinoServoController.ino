/*

  Prototype Code for Opto project - a framework for controlling 
  realworld objects with pixels.
  
  (c) binaura.net / 2015

*/


#include <Servo.h>
Servo s2, s4, s7, s8, s12, s13;

int currentValue = 0;
int values[] = {0,0};

void setup() {
   s2.attach(2);
   s4.attach(4);
   s7.attach(7);
   s8.attach(8);
   s12.attach(12);
   s13.attach(13);
   
   pinMode(3,OUTPUT);
   pinMode(5,OUTPUT);
   pinMode(6,OUTPUT);
   pinMode(9,OUTPUT);
   pinMode(10,OUTPUT);
   pinMode(11,OUTPUT);
   
   Serial.begin(115200); 
}

void loop() {
   // this is used instead of flush since arduino 1.0
   while(Serial.available()>0) {
    int incomingValue = Serial.read();
    values[currentValue] = incomingValue;
    currentValue++;
    
    if(currentValue > 1){
      currentValue = -1;
    }
    
    if(values[1] != -1){
      //  servo
      if(values[0] == 2){
        s2.write(values[1]);
      }else if(values[0] == 4){
        s4.write(values[1]);
      }else if(values[0] == 7){
        s7.write(values[1]);
      }else if(values[0] == 8){
        s8.write(values[1]);
      }else if(values[0] == 12){
        s12.write(values[1]);
      }else if(values[0] == 13){
        s13.write(values[1]);
      }
    
      //  analog (pwm)
      else if(values[0] == 3) {
        analogWrite(3, values[1]);
      }else if(values[0] == 5) {
        analogWrite(5, values[1]);
      }else if(values[0] == 6) {
        analogWrite(6, values[1]);
      }else if(values[0] == 9) {
        analogWrite(9, values[1]);
      }else if(values[0] == 10) {
        analogWrite(10, values[1]);
      }else if(values[0] == 11) {
        analogWrite(11, values[1]);
      }
    
      //  initial blink on pin_11
      else if(values[0] == 42) {
        analogWrite(11,255);
        delay(300);
        analogWrite(11,0);
      }
    }
    //Serial.println(values[0] + " " + values[1]);
    //Serial.flush(); 
  }
   delay(1); 
   
}

//
//  OptoDraggable.h
//  Opto
//
//  Created by bence samu on 06/03/15.
//
//

#ifndef Opto_OptoDraggable_h
#define Opto_OptoDraggable_h

#include "ofMain.h"

class Draggable
{
public:
    
    void setupView(ofRectangle _view)
    {
        view = _view;
        tl.set(view.x, view.y);
        br.set(view.x + view.width, view.y + view.height);
        scalerRad = 10;
        scaler = NULL;
        useScaler = false;
    }
    
    bool isPointOver(ofVec2f p)
    {
        ofRectangle v = view;
        v.scaleFromCenter(1 + scalerRad * 2 / view.width, 1 + scalerRad * 2 / view.height);
        return v.inside(p);
    }
    
    void onTouch(ofVec2f p)
    {
        if (useScaler && p.distance(tl) < scalerRad) scaler = &tl;
        else if (useScaler && p.distance(br) < scalerRad) scaler = &br;
        else
            touchPoint = p - ofVec2f(view.x, view.y);
    }
    
    void onDrag(ofVec2f p)
    {
        if (scaler)
        {
            *scaler = p;
            calcView();
        }
        else
        {
            view.x = p.x - touchPoint.x;
            view.y = p.y - touchPoint.y;
            tl.set(view.x, view.y);
            br.set(view.x + view.width, view.y + view.height);
        }
        
    }
    
    void onRelease(ofVec2f p)
    {
        scaler = NULL;
    }
    
    void calcView()
    {
        if (br.x < tl.x)
        {
            float tmp = br.x;
            br.x = tl.x;
            tl.x = tmp;
        }
        if (br.y < tl.y)
        {
            float tmp = br.y;
            br.y = tl.y;
            tl.y = tmp;
        }
        
        
        view.set(tl.x, tl.y, br.x - tl.x, br.y - tl.y);
    }
    
    void drawScaler()
    {
        if (useScaler)
        {
            ofSetColor(255, 200);
            ofFill();
            ofRect(tl.x - scalerRad, tl.y - scalerRad, scalerRad * 2, scalerRad * 2);
            ofSetColor(0, 200);
            ofNoFill();
            ofRect(tl.x - scalerRad, tl.y - scalerRad, scalerRad * 2, scalerRad * 2);
            
            ofSetColor(255, 200);
            ofFill();
            ofRect(br.x - scalerRad, br.y - scalerRad, scalerRad * 2, scalerRad * 2);
            ofSetColor(0, 200);
            ofNoFill();
            ofRect(br.x - scalerRad, br.y - scalerRad, scalerRad * 2, scalerRad * 2);
        }
    }
    
    ofRectangle view;
    ofVec2f touchPoint;
    
    ofVec2f tl, br;
    ofVec2f * scaler = NULL;
    float scalerRad;
    bool useScaler;

};


#endif

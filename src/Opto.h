//
//  Opto.h
//  Opto
//
//  Created by bence samu on 06/03/15.
//
//

#ifndef __Opto__Opto__
#define __Opto__Opto__

#include "ofMain.h"
#include "ElementBase.h"
#include "Sensor.h"
#include "OutputBase.h"
#include "Splash.h"

class Opto
{
public:
    
    void setup(OutputBase * _output);
    void update();
    void draw();
    void drawElementsGUI();
    
    void onPress(ofVec2f p);
    void onDrag(ofVec2f p);
    void onRelease(ofVec2f p);
    void onMoved(ofVec2f p);
    
    Draggable * getDraggable(ofVec2f p);
    ElementBase * getElement(ofVec2f p);    
    void removeElememt(ElementBase * e);
    
    ElementBase * addImage(string fileName, ofVec2f p);
    ElementBase * addMovie(string fileName, ofVec2f p);
    ElementBase * addGrabber(ofVec2f p);
    ElementBase * addMic(ofVec2f p);
    ElementBase * addSolid(ofVec2f p);
    
    Sensor * addSensor(int id, int _type, ofVec2f p);
    
    vector<ElementBase*> elements;
    vector<Sensor*> sensors;
    
    Draggable * cDraggable;
    OutputBase * output;
    
    bool useSplash;
    Splash splash;
    //static void changeInput(int in);
    
    ofTrueTypeFont myFont;
};

#endif /* defined(__Opto__Opto__) */

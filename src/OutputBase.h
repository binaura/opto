//
//  OutputBase.h
//  Opto
//
//  Created by bence samu on 12/03/15.
//
//

#ifndef Opto_OutputBase_h
#define Opto_OutputBase_h

#include "ofMain.h"
#include "Sensor.h"

class OutputBase
{
public:
    virtual void sendData(SensorData data){};
    virtual void changeOutput(int portNumber){};
    ofSerial serial;
};
#endif

//
//  ElementSolid.cpp
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#include "ElementSolid.h"

ElementSolid::ElementSolid(ofVec2f p)
{
    brightness = 0;
    baseSetup(p);
}
void ElementSolid::draw()
{
    ofSetColor(brightness * 255);
    ofFill();
    ofRect(view);
}

void ElementSolid::drawGUI()
{
    GUI::begin(ofRectangle(view.x, view.y + view.height, view.width , GUI::getGroupHeight(1)), true);
    GUI::doSlider(&brightness, 0, 1, "brightness");
}

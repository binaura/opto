//
//  OptoElement.h
//  Opto
//
//  Created by bence samu on 06/03/15.
//
//

#ifndef __Opto__OptoElement__
#define __Opto__OptoElement__

#include "ofMain.h"
#include "Draggable.h"
#include "GUI.h"

enum
{
    OPTO_ELEMENT_IMAGE,
    OPTO_ELEMENT_MOVIE,
    OPTO_ELEMENT_GRABBER,
    OPTO_ELEMENT_MIC,
    OPTO_ELEMENT_SOLID
};

class ElementBase : public Draggable
{
public:
    virtual ofTexture * getTexture() {return NULL;};
    virtual void update(){};
    virtual void draw(){};
    virtual void drawGUI(){};
    
    void baseSetup(ofVec2f p);
    void baseDrawGUI();
    
    static ofVec2f pointer;
    int type;
};

#endif /* defined(__Opto__OptoElement__) */

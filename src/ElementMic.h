//
//  ElementMic.h
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#ifndef __Opto__ElementMic__
#define __Opto__ElementMic__

#include "ofMain.h"
#include "ElementBase.h"
#include "ofxFFTLive.h"

class ElementMic : public ElementBase
{
public:
    ElementMic(ofVec2f p);
    ~ElementMic();
    static void init();
    static void staticUpdate();
    void update();
    void draw();
    void drawGUI();
    
    static ofxFFTLive fftLive;
    float numBins;
};

#endif /* defined(__Opto__ElementMic__) */

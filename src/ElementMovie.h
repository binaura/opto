//
//  ElementMovie.h
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#ifndef __Opto__ElementMovie__
#define __Opto__ElementMovie__

#include "ofMain.h"
#include "ElementBase.h"

class ElementMovie : public ElementBase
{
public:
    ElementMovie(string fileName, ofVec2f p);
    void update();
    void draw();
    void drawGUI();
    ofVideoPlayer vPlayer;
    float speed;
};

#endif /* defined(__Opto__ElementMovie__) */

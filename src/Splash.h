//
//  Splash.h
//  Opto
//
//  Created by Agoston Nagy on 16/03/15.
//
//

#ifndef __Opto__Splash__
#define __Opto__Splash__

#include <stdio.h>

#endif /* defined(__Opto__Splash__) */

#pragma once

#include "ofMain.h"
#include "GUI.h"
#include "OutputBase.h"
#include "Globals.h"

class Splash{
    public:
        void setup(OutputBase * _output);
        void draw();
        void pressed(ofVec2f p);
        void onMoved(ofVec2f p);
        vector<string> ports;
        vector<string> boards;
    
        ofVec2f pos;
    
        int selectedBoard;
        int selectedPort;
    
        OutputBase * output;
        ofTrueTypeFont myFont;
};
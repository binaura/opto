//
//  OutputSerial.h
//  Opto
//
//  Created by bence samu on 12/03/15.
//
//

#ifndef __Opto__OutputSerial__
#define __Opto__OutputSerial__

#include "ofMain.h"
#include "OutputBase.h"

class OutputSerial : public OutputBase
{
public:
    OutputSerial();
    ~OutputSerial();
    void sendData(SensorData data);
    void changeOutput(int portNumber);
    void debug();

    int baud;
    bool inited;
    vector <ofSerialDeviceInfo> deviceList;
    ofSerial serial;
    string ofxGetSerialString(ofSerial &serial, char until);
    string ofxTrimStringRight(string str);
    string ofxTrimStringLeft(string str);
    string ofxTrimString(string str);
};

#endif /* defined(__Opto__OutputSerial__) */

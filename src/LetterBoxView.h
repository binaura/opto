#ifndef __LetterBoxView_h__
#define __LetterBoxView_h__

#include "ofMain.h"

class LetterBoxView
{
public:
    
    static void begin(ofRectangle src, ofRectangle dst = ofGetCurrentViewport())
    {
        ofRectangle scaled = src;
        scaled.scaleTo(dst, OF_ASPECT_RATIO_KEEP, OF_ALIGN_HORZ_CENTER, OF_ALIGN_VERT_CENTER, OF_ALIGN_HORZ_CENTER, OF_ALIGN_VERT_CENTER);
        ofPushMatrix();
        ofTranslate(scaled.x, scaled.y);
        ofScale(scaled.width / src.width, scaled.height / src.height);
    };
    
    static void end(bool useMask = false, ofRectangle src = ofGetCurrentViewport(), ofRectangle dst = ofGetCurrentViewport())
    {
        ofPopMatrix();
        if (useMask)
        {
            ofRectangle scaled = src;
            scaled.scaleTo(dst, OF_ASPECT_RATIO_KEEP, OF_ALIGN_HORZ_CENTER, OF_ALIGN_VERT_CENTER, OF_ALIGN_HORZ_CENTER, OF_ALIGN_VERT_CENTER);
            ofFill();
            ofRect(0, 0, ofGetWidth(), scaled.y);
            ofRect(0, 0, scaled.x, ofGetHeight());
            ofRect(0, scaled.y + scaled.height, ofGetWidth(), ofGetHeight() - scaled.y - scaled.height);
            ofRect(scaled.x + scaled.width, 0, ofGetWidth() - scaled.x - scaled.width, ofGetHeight());
        }
    };
    
    static ofVec2f screenToLocal(ofVec2f p, ofRectangle src, ofRectangle dst = ofGetCurrentViewport())
    {
        ofRectangle scaled = src;
        scaled.scaleTo(dst, OF_ASPECT_RATIO_KEEP, OF_ALIGN_HORZ_CENTER, OF_ALIGN_VERT_CENTER, OF_ALIGN_HORZ_CENTER, OF_ALIGN_VERT_CENTER);
        
        return ofVec2f(ofMap(p.x, scaled.x, scaled.x + scaled.width, src.x, src.x + src.width),
                       ofMap(p.y, scaled.y, scaled.y + scaled.height, src.y, src.y + src.height));
    }
    
    static ofVec2f localToScreen(ofVec2f p, ofRectangle src, ofRectangle dst = ofGetCurrentViewport())
    {
        ofRectangle scaled = src;
        scaled.scaleTo(dst, OF_ASPECT_RATIO_KEEP, OF_ALIGN_HORZ_CENTER, OF_ALIGN_VERT_CENTER, OF_ALIGN_HORZ_CENTER, OF_ALIGN_VERT_CENTER);
        
        return ofVec2f(ofMap(p.x, src.x, src.x + src.width, scaled.x, scaled.x + scaled.width),
                       ofMap(p.y, src.y, src.y + src.height, scaled.y, scaled.y + scaled.height));
    }
};


#endif
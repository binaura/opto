//
//  ElementMic.cpp
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#include "ElementMic.h"

ofxFFTLive ElementMic::fftLive;

void ElementMic::init()
{
    fftLive.setMirrorData(false);
    //fftLive.setThreshold(0);
    //fftLive.setMaxDecay(0);
    fftLive.setPeakDecay(0);
    fftLive.setup();
}

void ElementMic::staticUpdate()
{
    fftLive.update();
}

ElementMic::ElementMic(ofVec2f p)
{
    numBins = 515;
    baseSetup(p);
    type = OPTO_ELEMENT_MIC;
}

void ElementMic::update()
{
    numBins = (int)numBins;

}

void ElementMic::draw()
{
    int num = numBins * 5;  // ignore high freqs
    float * audioData = new float[num];
    fftLive.getFftPeakData(audioData, num);
    
    ofFill();
    ofSetColor(0);
    ofRect(view);
    for (int i = 0; i < numBins; i++)
    {
        float audioValue = audioData[i];
        ofSetColor(255);
        ofRect(ofMap(i, 0, numBins, view.x, view.x + view.width), view.y + view.height, view.width / (numBins), -view.height * audioValue);
    }
}

void ElementMic::drawGUI()
{
    GUI::begin(ofRectangle(view.x, view.y + view.height, view.width , GUI::getGroupHeight(1)), true);
    GUI::doSlider(&numBins, 1, 100, "bins");
}


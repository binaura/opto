//
//  ElementSolid.h
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#ifndef __Opto__ElementSolid__
#define __Opto__ElementSolid__

#include "ofMain.h"
#include "ElementBase.h"

class ElementSolid : public ElementBase
{
public:
    ElementSolid(ofVec2f p);
    void draw();
    void drawGUI();
    
    float brightness;
};

#endif /* defined(__Opto__ElementSolid__) */

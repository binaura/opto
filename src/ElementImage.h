//
//  ElementImage.h
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#ifndef __Opto__ElementImage__
#define __Opto__ElementImage__

#include "ofMain.h"
#include "ElementBase.h"

class ElementImage : public ElementBase
{
public:
    ElementImage(string fileName, ofVec2f p);
    void draw();
    void drawGUI();
    void update();
    ofTexture texture;
    
    float speed;
    float shift;
    float rotate;
};

#endif /* defined(__Opto__ElementImage__) */

//
//  Globals.h
//  Opto
//
//  Created by Agoston Nagy on 26/03/15.
//
//

#ifndef Opto_Globals_h
#define Opto_Globals_h
#include "ofMain.h"

class Globals{
public:
    static ofTrueTypeFont largeFont;
    static ofTrueTypeFont smallFont;
    static ofTrueTypeFont miniFont;
    
    static ofTexture texIcons[5];
    
    enum
    {
        ICO_SOLID,
        ICO_IMAGE,
        ICO_MOVIE,
        ICO_CAMERA,
        ICO_MIC
    };
};


#endif

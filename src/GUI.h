//
//  GUI.h
//  Faces
//
//  Created by bence samu on 10/02/15.
//
//

#ifndef __Faces__GUI__
#define __Faces__GUI__

#include "ofMain.h"
#include "Globals.h"


class GUI
{
public:
    
    static void reset();
    static bool isOver();
    static float getGroupHeight(int n);
    static void begin(ofRectangle _view, bool visible);
    
    static bool doSlider(float * thiz, float vMin, float vMax, string text);
    static bool doToggle(float * thiz, string text);
    static bool doRangeSlider(float * thiz, float vMin, float vMax, float * range, string text);
        
    static void * hot;
    static void * active;
    static ofRectangle groupView;
    static ofVec2f pos;
    static bool display;
    static float border;
    static float spacing;
    static float elementHeight;
    static ofVec2f pointer;
    
    static bool started;
};


#endif /* defined(__Faces__GUI__) */

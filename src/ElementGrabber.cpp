//
//  ElementGrabber.cpp
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#include "ElementGrabber.h"
#include "LetterBoxView.h"

ofVideoGrabber ElementGrabber::vGrabber;
ofxCvColorImage ElementGrabber::cvColor;
ofEvent<bool> ElementGrabber::newFrameEventArgs;

ElementGrabber::ElementGrabber(ofVec2f p)
{
    if (!vGrabber.isInitialized())
    {
        vGrabber.setUseTexture(false);
        vGrabber.initGrabber(320, 240);
        cvColor.setUseTexture(false);
        cvColor.allocate(vGrabber.getWidth(), vGrabber.getHeight());
        cvGray.setUseTexture(false);
        cvGray.allocate(vGrabber.getWidth(), vGrabber.getHeight());
        cvFloat.allocate(vGrabber.getWidth(), vGrabber.getHeight());
        cvGrayBg.allocate(vGrabber.getWidth(), vGrabber.getHeight());
        texture.allocate(vGrabber.getWidth(), vGrabber.getHeight(), GL_LUMINANCE);
    }
    blur = 0;
    threshold = 0;
    adapt = 0;
    amplify = 0;
    invert = 0;
    baseSetup(p);
    type = OPTO_ELEMENT_GRABBER;
}

void ElementGrabber::staticUpdate()
{
    if (vGrabber.isInitialized())
    {
        vGrabber.update();
        if (vGrabber.isFrameNew())
        {
            cvColor.setFromPixels(vGrabber.getPixels().getData(), vGrabber.getWidth(), vGrabber.getHeight());
            bool e = true;
            ofNotifyEvent(newFrameEventArgs, e);
        }
    }
}

void ElementGrabber::updatePixels(bool & e)
{
    cvGray = cvColor;
    if (invert > 0) cvGray.invert();
    if (adapt > 0)
    {
        cvFloat.addWeighted(cvGray, powf(adapt, 4));
        cvConvertScale(cvFloat.getCvImage(), cvGrayBg.getCvImage(), 255.0f/65535.0f, 0);
        cvGrayBg.flagImageChanged();
        cvGray.absDiff(cvGrayBg);
    }
    else cvGrayBg = cvGray;
    if (amplify > 0) cvMul(cvGray.getCvImage(), cvGray.getCvImage(), cvGray.getCvImage(), amplify);
    if (blur > 0) cvGray.blur((int)blur * 2 + 1);
    if (threshold > 0) cvGray.threshold((int)threshold);
   
    texture.loadData(cvGray.getPixels().getData(), cvGray.width, cvGray.height, GL_LUMINANCE);
}

void ElementGrabber::draw()
{
    LetterBoxView::begin(ofRectangle(0, 0, vGrabber.getWidth(), vGrabber.getHeight()), view);
    ofSetColor(255);
    texture.draw(0, 0);
    LetterBoxView::end();
}

void ElementGrabber::drawGUI()
{
    GUI::begin(ofRectangle(view.x, view.y + view.height, view.width , GUI::getGroupHeight(5)),true);
    GUI::doToggle(&invert, "invert");
    GUI::doSlider(&adapt, 0, 0.5, "adapt");
    GUI::doSlider(&amplify, 0, 1, "amplify");
    GUI::doSlider(&blur, 0, 20, "blur");
    GUI::doSlider(&threshold, 0, 255, "threshold");
}

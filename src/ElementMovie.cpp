//
//  ElementMovie.cpp
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#include "ElementMovie.h"
#include "LetterBoxView.h"

ElementMovie::ElementMovie(string fileName, ofVec2f p)
{
    vPlayer.loadMovie(fileName);
    vPlayer.setLoopState(OF_LOOP_NORMAL);
    vPlayer.play();
    speed = 1;
    baseSetup(p);
    type = OPTO_ELEMENT_MOVIE;
}
void ElementMovie::draw()
{
    LetterBoxView::begin(ofRectangle(0, 0, vPlayer.getWidth(), vPlayer.getHeight()), view);
    ofSetColor(255);
    vPlayer.draw(0, 0);
    LetterBoxView::end();
}

void ElementMovie::drawGUI()
{
    GUI::begin(ofRectangle(view.x, view.y + view.height, view.width , GUI::getGroupHeight(1)), true);
    GUI::doSlider(&speed, -2, 2, "speed");
}

void ElementMovie::update()
{
    vPlayer.setSpeed(speed);
    vPlayer.update();
}

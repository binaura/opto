//
//  Splash.cpp
//  Opto
//
//  Created by Agoston Nagy on 16/03/15.
//
//

#include "Splash.h"

void Splash::setup(OutputBase * _output){
    output = _output;
    selectedBoard = 0;
    selectedPort = 0;
}
void Splash::draw(){
    ofSetColor(0,134,140);
    ofFill();
    ofRect(0,0,ofGetWidth(),ofGetHeight());
    ofSetColor(255);

    ofSetColor(255);
    Globals::largeFont.drawString("SELECT YOUR BOARD", 100,100);


    for( int i=0; i< boards.size(); i++ ) {
        ofVec2f tp = ofVec2f(110,130 + i * 20);
        if(pos.distance(tp)<10) {
            ofSetColor(237,167,0);
        }else{
            ofSetColor(255);
        }
        Globals::smallFont.drawString((selectedBoard == i ? "[x] " : "[ ] ") + boards[i], 100,130 + i * 20);
    }

    ofSetColor(255);
    Globals::largeFont.drawString("SELECT YOUR PORT", ofGetWidth() / 2,100);
    if (ports.size())
    {
    for( int i=0; i< ports.size(); i++ ) {
        ofVec2f tp = ofVec2f(ofGetWidth()/2 + 10,130 + i * 20);
        if(pos.distance(tp)<10) {
            ofSetColor(237,167,0);
        }else{
            ofSetColor(255);
        }
        Globals::smallFont.drawString((selectedPort == i ? "[x] " : "[ ] ") + ports[i], ofGetWidth()/2,130 + i * 20);
    }

    if(selectedBoard>=0){
        ofSetColor(10,200,200);
        Globals::largeFont.drawString("SELECTED BOARD IS: " + boards[selectedBoard],100,ofGetHeight()-150);
    }
    if(selectedPort>=0){
        ofSetColor(10,200,200);
        Globals::largeFont.drawString("SELECTED PORT IS: " + ports[selectedPort],ofGetWidth() / 2, ofGetHeight()-150);
    }
    }
    ofVec2f tok = ofVec2f(110, ofGetHeight()-100);
    if(pos.distance(tok)<30){
        ofSetColor(237,167,0);
    }
    else{
         ofSetColor(255);
        }
    Globals::largeFont.drawString("[ ] LET'S START", 100, ofGetHeight()-100);
}

void Splash::pressed(ofVec2f p){
    for( int i=0; i< boards.size(); i++ ) {
        ofVec2f tp = ofVec2f(110,120 + i * 20);
        if(p.distance(tp)<10) {
            selectedBoard = i;
        }
    }
    for( int i=0; i< ports.size(); i++ ) {
        ofVec2f tp = ofVec2f(ofGetWidth()/2 + 10,120 + i * 20);
        if(p.distance(tp)<10) {
            selectedPort = i;
        }
    }
    ofVec2f tok = ofVec2f(110, ofGetHeight()-100);
    if(p.distance(tok)<30){
        // todo ::   send board here, too
        output->changeOutput(selectedPort);
        GUI::started = true;
    }
}

void Splash::onMoved(ofVec2f p){
    pos = p;
}

#include "ofApp.h"
#include "GUI.h"
#include "OutputSerial.h"

/*
TODO:
 + sensor: use loadscreendata brightness average 
 - arduinoType / decided based on pin numbers (data.id)
 + implement switch
    + add sub sensor and mltiply values
    + connect with lines
 + use Output class to interface width Arduino, OSC, etc
 + load image / movie from finder
 + add view scaling
 
*/

ofTrueTypeFont Globals::largeFont;
ofTrueTypeFont Globals::smallFont;
ofTrueTypeFont Globals::miniFont;
ofTexture Globals::texIcons[];

//--------------------------------------------------------------
void ofApp::setup()
{
    
    opto.setup(new OutputSerial());
    
    Globals::largeFont.loadFont("assets/dinBold.ttf", 18, true, true);
    Globals::largeFont.setLineHeight(18.0f);
    Globals::largeFont.setLetterSpacing(1.1);
    
    Globals::smallFont.loadFont("assets/dinBold.ttf", 14, true, true);
    Globals::smallFont.setLineHeight(18.0f);
    Globals::smallFont.setLetterSpacing(1.1);
    
    Globals::miniFont.loadFont("assets/dinBold.ttf", 12, true, true);
    Globals::miniFont.setLineHeight(18.0f);
    Globals::miniFont.setLetterSpacing(1.1);
    
    ofLoadImage(Globals::texIcons[Globals::ICO_MIC], "assets/mic.jpg");
    ofLoadImage(Globals::texIcons[Globals::ICO_SOLID], "assets/solid.jpg");
    ofLoadImage(Globals::texIcons[Globals::ICO_IMAGE], "assets/image.jpg");
    ofLoadImage(Globals::texIcons[Globals::ICO_MOVIE], "assets/movie.jpg");
    ofLoadImage(Globals::texIcons[Globals::ICO_CAMERA], "assets/camera.jpg");
}

//--------------------------------------------------------------
void ofApp::update()
{
    GUI::pointer = ofVec2f(mouseX, mouseY);
    opto.update();
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofBackground(220);
    opto.draw();
    
    ofSetColor(0, 200); ofFill(); ofCircle(mouseX, mouseY, 3);
    ofSetColor(255); ofNoFill(); ofCircle(mouseX, mouseY, 3);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    ofVec2f p = ofVec2f(mouseX, mouseY);
    if (key == 'i')
    {
        ofFileDialogResult fd = ofSystemLoadDialog("", "");
        if (ofToLower(fd.fileName).find("jpg") != std::string::npos || ofToLower(fd.fileName).find("png") != std::string::npos)
        {
            opto.addImage(fd.fileName, p);
        }
    }
    if (key == 'v')
    {
        ofFileDialogResult fd = ofSystemLoadDialog("", "");
        if (ofToLower(fd.fileName).find("mov") != std::string::npos)
        {
            opto.addMovie(fd.fileName, p);
        }
    }
    if (key == 'g')
    {
        opto.addGrabber(p);
    }
    if (key == 'm')
    {
        opto.addMic(p);
    }
    if (key == 's')
    {
        opto.addSolid(p);
    }

    if (key == OF_KEY_BACKSPACE)
    {
        opto.removeElememt(opto.getElement(p));
    }
    
    if (key == 'x')
    {
        for (int i = 0; i < opto.sensors.size(); i++)
        {
            if (opto.sensors[i]->isPointOverWire(p))
            {
                opto.sensors[i]->addSwitch(opto.addSensor(0, Sensor::SENSOR_GATE, p));
                break;
            }
        }
    }
    
    /*
     
    //  switch between serial ports by pressing keys (0-9)
    //  if port is ok, pin11 will blink
    
    for(int i=0; i< 10; i++) {
        char myChar = '0' + i;
        if( key == myChar ) {
            opto.output->changeOutput(i);
        }
    }
     
    */
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y )
{
    opto.onMoved(ofVec2f(x, y));
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
    opto.onDrag(ofVec2f(x, y));
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    opto.onPress(ofVec2f(x, y));
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
    opto.onRelease(ofVec2f(x, y));
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//
//  Sensor.cpp
//  Opto
//
//  Created by bence samu on 06/03/15.
//
//

#include "Sensor.h"

Sensor::Sensor(int _id, int _type, ofVec2f _p)
{
    base = _p;

    data.id = _id;
    data.type = _type;
    radius = _type != SENSOR_GATE ? 20 : 10;
    setupView(ofRectangle(_p.x - radius, _p.y - radius, radius * 2, radius * 2));

    data.value = 0;
    valueSmoothing = 0.1;
    
    area = 4;
    pixels.allocate(area, area, OF_IMAGE_COLOR);
    texture.allocate(pixels);
    
    data.rMin = 0;
    if(data.type==SENSOR_SERVO){
        data.rMax = 180;
    }else if(data.type == SENSOR_PWM){
        data.rMax = 255;
    }
    
#ifdef USE_FUNKY_SOUND
    sample.loadSound("sine.aiff");
    sample.setLoop(true);
    sample.setVolume(0);
    sample.play();
#endif
}

void Sensor::update()
{
    // RED = empty
    unsigned char RGB[3];
    glReadPixels(view.x + view.width / 2., ofGetHeight() - view.y - view.height / 2., 1,1, GL_RGB, GL_UNSIGNED_BYTE, RGB);
    if (RGB[0] == 255 && RGB[1] == 0 && RGB[2] == 0)
    {
        on = false;
    }
    else
    {
        on = true;
        ofVec2f center = view.getCenter();
        texture.loadScreenData(center.x - area / 2, center.y - area / 2, area, area);
        texture.readToPixels(pixels);
        float brightness = 0;
        for (int i = 0; i < area * area; i++)
        {
            brightness += pixels.getPixels()[i * 3 + 0];
        }
        brightness /= (area * area);
        
        data.value += (ofMap(brightness, 0, 255, 0, 1, true) - data.value) * ofMap(valueSmoothing, 0, 1, 1, 0.001);
        
        for (int i = 0; i < switches.size(); i++)
        {
            data.value *= switches[i]->data.value;
        }
    }
    
    if (data.type != SENSOR_GATE)
    {
        wire.clear();
        wire.addVertex(base);
        for (int i = 0; i < switches.size(); i++)
        {
            wire.addVertex(switches[i]->view.getCenter());
        }
        wire.addVertex(view.getCenter());
    }
    else
    {
        on = false;
        base = view.getCenter();
    }
    
#ifdef USE_FUNKY_SOUND
    if (on)
    {
        sample.setSpeed(ofMap(data.value, 0, 1, 0.1, 3));
        sample.setVolume(0.3);
    }
    else
    {
        sample.setVolume(0);
    }
#endif
}

void Sensor::draw()
{
    if (data.type != SENSOR_GATE)
    {
        ofSetColor(0,97,111,100);
        ofSetLineWidth(5);
        wire.draw();
        ofSetLineWidth(1);
    }
    
    ofMesh mesh;
    mesh.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    float r = 5;
    float angleStep = TWO_PI / 20.0;
    for (float a = 0; a < TWO_PI + angleStep; a += angleStep)
    {
        mesh.addVertex(view.getCenter() + ofVec2f(cos(a), sin(a)) * r);
        mesh.addVertex(view.getCenter() + ofVec2f(cos(a), sin(a)) * radius);
    }
    
    ofColor color = ofColor(67, 170, 164);
    if (data.type == SENSOR_GATE)
        color = ofColor(240,96,40);
    
    ofSetColor(255);
    mesh.draw();
    
    ofSetColor(color, 200);
    ofNoFill();
    ofSetLineWidth(3);
    ofCircle(view.getCenter(), radius);
    //ofCircle(view.getCenter(), r);
    ofFill();
    
    ofSetColor(data.value * 255);
    
    ofCircle(view.getCenter(),radius / 5);
    ofSetLineWidth(1);
    ofNoFill();
    ofSetColor(255);
    string text = "PWM";
    if (data.type == SENSOR_SERVO) text = "SERVO";
    else if (data.type == SENSOR_GATE) text = "GATE";
    //text += " : ";
    
    if(data.type == SENSOR_GATE) {
        Globals::smallFont.drawString(text, view.x, view.y-40);
        Globals::smallFont.drawString(ofToString(data.id),view.x, view.y - 25);

        ofRect(view.x, view.y-10, view.width, 5);
        ofSetColor(color, 240);
        ofRect(view.x, view.y-10, view.width * data.value, 1);
        
    }else{
        Globals::smallFont.drawString(text, base.x - 220, base.y);
        Globals::smallFont.drawString(ofToString(data.id),base.x - 220, base.y + 20);

        ofRect(base.x - 220, base.y, view.width, 5);
        ofSetColor(color, 240);
        ofRect(base.x - 220, base.y + 2, view.width * data.value, 1);
    }
    
    GUI::begin(ofRectangle(base.x + -150, base.y - 20, 80 , 60),false);
    
    if(data.type==SENSOR_SERVO){
        GUI::doRangeSlider(&data.rMin, 0, 180, &data.rMax, "min" );
        GUI::doRangeSlider(&data.rMax, 0, 180, &data.rMin, "max" );
    }else if (data.type==SENSOR_PWM){
        GUI::doRangeSlider(&data.rMin, 0, 255, &data.rMax, "min" );
        GUI::doRangeSlider(&data.rMax, 0, 255, &data.rMin, "max" );
    }
}

void Sensor::addSwitch(Sensor *s)
{
    switches.push_back(s);
}

bool Sensor::isPointOverWire(ofVec2f p)
{
    return wire.getClosestPoint(p).distance(p) < 10;
}
//
//  OutputSerial.cpp
//  Opto
//
//  Created by bence samu on 12/03/15.
//
//

#include "OutputSerial.h"

OutputSerial::OutputSerial()
{
    serial.listDevices();
    deviceList = serial.getDeviceList();
    baud = 115200;
    serial.setup(0, baud);

    //  checking connection > lighting led on pin 11
    Byte out[2];
    out[0] = Byte(42);
    out[1] = Byte(255);
    serial.writeBytes(&out[0], 2);
}

OutputSerial::~OutputSerial()
{
    serial.close();
}

void OutputSerial::debug(){
    string str = ofxGetSerialString(serial, '\n');
    if (str=="") {
        //geen nieuws
    } else {
        //str bevat 1 regel tekst van Arduino
        printf("arduino says: %s\n", str.c_str());
    }
}

void OutputSerial::sendData(SensorData data) {
    int pinNumber = data.id;
    int pinValue = 0;

    pinValue = ofMap(data.value,0,1,data.rMin,data.rMax);

    /*if (data.type == Sensor::SENSOR_SERVO) {
        pinValue = ofMap(data.value,0,1,0,180);
    }
    else if (data.type == Sensor::SENSOR_PWM) {
        pinValue = ofMap(data.value,0,1,0,255);
    }*/

    //printf("id: %d value: %d \n", pinNumber, pinValue);
    Byte out[2];
    out[0] = Byte(pinNumber);
    out[1] = Byte(pinValue);
    serial.writeBytes(&out[0], 2);
}

string OutputSerial::ofxGetSerialString(ofSerial &serial, char until) {
    static string str;
    stringstream ss;
    char ch;
    int ttl=1000;
    while ((ch=serial.readByte())>0 && ttl-->0 && ch!=until) {
        ss << ch;
    }
    str+=ss.str();
    if (ch==until) {
        string tmp=str;
        str="";
        return ofxTrimString(tmp);
    } else {
        return "";
    }
}

string OutputSerial::ofxTrimStringRight(string str) {
    size_t endpos = str.find_last_not_of(" \t\r\n");
    return (string::npos != endpos) ? str.substr( 0, endpos+1) : str;
}

// trim trailing spaces
string OutputSerial::ofxTrimStringLeft(string str) {
    size_t startpos = str.find_first_not_of(" \t\r\n");
    return (string::npos != startpos) ? str.substr(startpos) : str;
}

string OutputSerial::ofxTrimString(string str) {
    return ofxTrimStringLeft(ofxTrimStringRight(str));;
}

void OutputSerial::changeOutput(int portNumber) {
    serial.close();
    serial.setup(portNumber,baud);

    //  checking connection > lighting led on pin 11
    Byte out[2];
    out[0] = Byte(42);
    out[1] = Byte(255);
    serial.writeBytes(&out[0], 2);
}

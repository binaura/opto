//
//  GUI.cpp
//  Faces
//
//  Created by bence samu on 10/02/15.
//
//

#include "GUI.h"

void * GUI::hot = NULL;
void * GUI::active = NULL;
ofVec2f GUI::pos;
ofRectangle GUI::groupView;
bool GUI::display = false;
float GUI::border = 5;
float GUI::spacing = 5;
float GUI::elementHeight = 10;
ofVec2f GUI::pointer;
bool GUI::started = false;

void GUI::reset()
{
    hot = active = NULL;
}
void GUI::begin(ofRectangle _view, bool visible)
{
    groupView = _view;
    pos.set(groupView.x + border, groupView.y + border);
    if(visible) {
        ofFill();
        ofSetColor(237,167,0);
        ofSetLineWidth(1);
        ofRectRounded(groupView, border / 2., border / 2., border / 2., border / 2.);
    
        ofNoFill();
        ofSetColor(237,167,0);
        ofRectRounded(groupView, border / 2.);
    }
}

float GUI::getGroupHeight(int n)
{
    return n * (elementHeight + spacing) + border * 2 - spacing;
}

bool GUI::isOver()
{
    return hot != NULL || GUI::active != NULL;
}

bool GUI::doToggle(float * thiz, string text)
{
    bool ret = false;
    
    ofRectangle view(pos.x, pos.y, groupView.width - border * 2, elementHeight);
    ofRectangle toggleView(pos.x + 2, pos.y + 2, elementHeight - 4, elementHeight - 4);
    
    if (GUI::active == thiz)
    {
        if (ofGetMousePressed())
        {
            ret = true;
        }
        else GUI::active = NULL;
    }
    else if (GUI::hot == thiz)
    {
        if (ofGetMousePressed() && GUI::active == NULL)
        {
            GUI::active = thiz;
            ret = true;
            *thiz = *thiz == 0 ? 1 : 0;
        }
        else GUI::hot = NULL;
    }
    if (view.inside(pointer) && GUI::hot == NULL && !ofGetMousePressed())
    {
        GUI::hot = thiz;
    }
    
    ofFill();
    ofSetColor(255, GUI::hot == thiz ? 150 : 100);
    ofRect(view);
    
    if (*thiz != 0)
    {
        ofSetColor(255);
        ofFill();
        ofRect(toggleView);
    }
    
    
    ofSetColor(255, 200);
    ofNoFill();
    ofRect(toggleView);
    
    ofSetColor(0, 200);
    Globals::smallFont.drawString(text, (int)view.x + 24, (int)(view.y + view.height - 2));
    
    pos.y += view.height + spacing;
    
    return ret;
}

bool GUI::doSlider(float * thiz, float vMin, float vMax, string text)
{
    bool ret = false;
    
    ofRectangle view(pos.x, pos.y, groupView.width - border * 2, elementHeight);
    
    if (GUI::active == thiz)
    {
        if (ofGetMousePressed())
        {
            ret = true;
            *thiz = ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true);
        }
        else GUI::active = NULL;
    }
    else if (GUI::hot == thiz)
    {
        if (ofGetMousePressed() && GUI::active == NULL)
        {
            GUI::active = thiz;
            ret = true;
            *thiz = ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true);
        }
        else GUI::hot = NULL;
    }
    if (view.inside(pointer) && GUI::hot == NULL && !ofGetMousePressed())
    {
        GUI::hot = thiz;
    }
    
    ofFill();
    ofSetColor(255, GUI::hot == thiz ? 150 : 100);
    ofRect(view);
    
    ofSetColor(0, 200);
    Globals::miniFont.drawString(text + " : " + ofToString(*thiz), (int)view.x + 4, (int)(view.y + view.height - 2));
    ofSetColor(255);
    float pct = ofMap(*thiz, vMin, vMax, 0, 1, true);
    ofRect(view.x + pct * view.width - 1, view.y, 3, view.height);
    
    pos.y += view.height + spacing;
    
    return ret;
}

bool GUI::doRangeSlider(float * thiz, float vMin, float vMax, float *range, string text) {
    bool ret = false;
    
    ofNoFill();
    ofSetColor(255);
    ofRectangle view(pos.x, pos.y, 80, 10);
    
    if (GUI::active == thiz) {
        if (ofGetMousePressed()) {
            ret = true;
            if(text == "min"){
                if(ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true)<=*range){
                    *thiz = ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true);
                }
            }else{
                if(ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true)>=*range){
                   *thiz = ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true);
                }
            }
            
        }
        else GUI::active = NULL;
    }
    else if (GUI::hot == thiz) {
        if (ofGetMousePressed() && GUI::active == NULL) {
            GUI::active = thiz;
            ret = true;
            if(text == "min"){
                if(ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true)<=*range){
                    *thiz = ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true);
                }
            }else{
                if(ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true)>=*range){
                   *thiz = ofMap(pointer.x, view.x, view.x + view.width, vMin, vMax, true);
                }
            }
        }
        else GUI::hot = NULL;
    }
    if (view.inside(pointer) && GUI::hot == NULL && !ofGetMousePressed()) {
        GUI::hot = thiz;
    }
    
    ofNoFill();
    ofRect(view);
    
    ofSetColor(0, 40);
    Globals::miniFont.drawString(ofToString(int(*thiz)), (int)(view.x + view.width + 4), (int)(view.y + view.height - 2));
    
    ofFill();
    ofSetColor(255,59,0,100);
    
    float pct = ofMap(*thiz, vMin, vMax, 0, 1, true);
    ofRect(view.x + pct * view.width - 1, view.y, 3, view.height);
    
    pos.y += view.height + spacing;
    
    return ret;
}

//
//  ElementGrabber.h
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#ifndef __Opto__ElementGrabber__
#define __Opto__ElementGrabber__

#include "ofMain.h"
#include "ElementBase.h"
#include "ofxOpenCv.h"

class ElementGrabber : public ElementBase
{
public:
    ElementGrabber(ofVec2f p);
    void updatePixels(bool & e);
    void draw();
    void drawGUI();
    static void staticUpdate();
    
    static ofVideoGrabber vGrabber;
    static ofxCvColorImage cvColor;
    static ofEvent<bool> newFrameEventArgs;
    ofxCvGrayscaleImage cvGray;
    ofxCvShortImage cvFloat;
    ofxCvGrayscaleImage cvGrayBg;
    float blur, threshold;
    float adapt, amplify;
    float invert;
    ofTexture texture;

};

#endif /* defined(__Opto__ElementGrabber__) */

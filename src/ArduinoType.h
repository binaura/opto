//
//  ArduinoType.h
//  Opto
//
//  Created by bence samu on 12/03/15.
//
//

#ifndef Opto_ArduinoType_h
#define Opto_ArduinoType_h

class ArduinoType
{
public:
    string name;
    vector<int> pinsPWM, pinsServo;
};

#endif

//
//  Sensor.h
//  Opto
//
//  Created by bence samu on 06/03/15.
//
//

#ifndef __Opto__Sensor__
#define __Opto__Sensor__

#include "ofMain.h"
#include "Draggable.h"
#include "Globals.h"
#include "Gui.h"

//#define USE_FUNKY_SOUND

class SensorData
{
public:
    int id;
    int type;
    float value;
    float rMin;
    float rMax;
};
class Sensor : public Draggable
{
public:
    enum {
        SENSOR_PWM,
        SENSOR_SERVO,
        SENSOR_GATE
    };
    Sensor( int _id, int _type, ofVec2f p);
    void update();
    void draw();
    void addSwitch(Sensor * s);
    bool isPointOverWire(ofVec2f p);
    
    SensorData data;
    
    float valueSmoothing;
    float radius;
    ofTexture texture;
    ofPixels pixels;
    bool on;
    vector<Sensor*> switches;
    ofPolyline wire;
    ofVec2f base;
    
    //float rMin, rMax;
    
    int area;
#ifdef USE_FUNKY_SOUND
    ofSoundPlayer sample;
#endif
};

#endif /* defined(__Opto__Sensor__) */

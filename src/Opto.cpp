//
//  Opto.cpp
//  Opto
//
//  Created by bence samu on 06/03/15.
//
//

#include "Opto.h"
#include "GUI.h"

#include "ElementImage.h"
#include "ElementMic.h"
#include "ElementMovie.h"
#include "ElementGrabber.h"
#include "ElementSolid.h"
#include "ArduinoType.h"

void Opto::setup(OutputBase * _output)
{
    cDraggable = NULL;
    
    output = _output;
    
    ElementMic::init();
    
    ArduinoType arduino;
    arduino.name = "arduino";
    arduino.pinsPWM.push_back(3);
    arduino.pinsPWM.push_back(5);
    arduino.pinsPWM.push_back(6);
    arduino.pinsPWM.push_back(9);
    arduino.pinsPWM.push_back(10);
    arduino.pinsPWM.push_back(11);
    
    arduino.pinsServo.push_back(2);
    arduino.pinsServo.push_back(4);
    arduino.pinsServo.push_back(7);
    arduino.pinsServo.push_back(8);
    arduino.pinsServo.push_back(12);
    arduino.pinsServo.push_back(13);
    
    ofVec2f p(230, 20);
    for (int i = 0; i < arduino.pinsPWM.size(); i++)
    {
        addSensor(arduino.pinsPWM[i], Sensor::SENSOR_PWM, p);
        p.y += 60;
    }
    
    p.y += 40;
    for (int i = 0; i < arduino.pinsServo.size(); i++)
    {
        addSensor(arduino.pinsServo[i], Sensor::SENSOR_SERVO, p);
        p.y += 60;
    }
    
    useSplash = true;
    
    if(useSplash){
        GUI::started = false;
    
        for(int i=0; i< output->serial.getDeviceList().size(); i++){
            string mydev = output->serial.getDeviceList()[i].getDeviceName();
            splash.ports.push_back(mydev);
        }

        splash.boards.push_back("UNO");
        splash.boards.push_back("LEONARDO");
        splash.boards.push_back("MEGA");
        splash.setup(output);
        
    }else{
        GUI::started = true;
    }
}

void Opto::update()
{
    // update elements
    ElementGrabber::staticUpdate();
    ElementMic::staticUpdate();
    
    if(GUI::started){
        for (int i = 0; i < elements.size(); i++){
            elements[i]->update();
        }
    
        // do picking "behind" the screen
        ofBackground(255, 0, 0);    // RED = empty
        for (int i = 0; i < elements.size(); i++){
            elements[i]->draw();
        }
    
        // send data out
        for (int i = 0; i < sensors.size(); i++){
            sensors[i]->update();
            if (sensors[i]->on) output->sendData(sensors[i]->data);
        }
    }
}

void Opto::draw(){
    
    // draw based on picking order
    if(GUI::started){
        
        //drawHint();
        drawElementsGUI();
        
        ofSetColor(67,170,164);
        ofRect(0,0,240,ofGetHeight());
        
        ofSetColor(13,121,127);
        ofRect(0,0,80,ofGetHeight());
        
        for (int i = 0; i < elements.size(); i++){
            elements[i]->draw();
        }
    
        for (int i = 0; i < sensors.size(); i++){
            sensors[i]->draw();
        }
    
        for (int i = 0; i < elements.size(); i++){
            elements[i]->baseDrawGUI();
        }
    }else{
        splash.draw();
    }
}

void Opto::drawElementsGUI()
{
    int h = 50;
    int xp = 250;
    int yp = ofGetHeight() - h + 5;
    int step = 80;
    
    int selectedElement = -1;
    if (ofGetMouseY() > ofGetHeight() - h)
    {
        selectedElement = (ofGetMouseX() - xp - step) / step;
    }

    ofSetColor(255);
    ofFill();
    ofRect(0,ofGetHeight()  - h, ofGetWidth(), h);
    
    ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);
    for (int i = 0; i < 5; i++)
    {
        i == selectedElement ? ofSetColor(255, 255, 0) : ofSetColor(255);
        Globals::texIcons[i].draw(xp += step, yp);
    }
    ofEnableAlphaBlending();
}

Draggable * Opto::getDraggable(ofVec2f p)
{
    for (int i = 0; i < sensors.size(); i++)
    {
        if (sensors[sensors.size() - 1 - i]->isPointOver(p)) return sensors[sensors.size() - 1 - i];
    }
    
    for (int i = 0; i < elements.size(); i++)
    {
        if (elements[elements.size() - 1 - i]->isPointOver(p)) return elements[elements.size() - 1 - i];
    }
    return NULL;
}

ElementBase * Opto::getElement(ofVec2f p)
{
    for (int i = 0; i < elements.size(); i++)
    {
        if (elements[i]->isPointOver(p)) return elements[i];
    }
    return NULL;
}

void Opto::removeElememt(ElementBase * e)
{
    for (vector<ElementBase*>::iterator i = elements.begin(); i != elements.end(); )
    {
        if (*i == e)
        {
            if ((*i)->type == OPTO_ELEMENT_GRABBER)
            {
                ElementGrabber * grabber = static_cast<ElementGrabber*>(*i);
                ofRemoveListener(ElementGrabber::newFrameEventArgs, grabber, &ElementGrabber::updatePixels);
            }
            delete *i;
            i = elements.erase(i);
        }
        else i++;
    }
}

ElementBase * Opto::addImage(string fileName, ofVec2f p)
{
    ElementImage * img = new ElementImage(fileName, p);
    elements.push_back(img);
    return img;
}

ElementBase * Opto::addMovie(string fileName, ofVec2f p)
{
    ElementMovie * mov = new ElementMovie(fileName, p);
    elements.push_back(mov);
    return mov;
}

ElementBase * Opto::addGrabber(ofVec2f p)
{
    ElementGrabber * grabber = new ElementGrabber(p);
    elements.push_back(grabber);
    ofAddListener(ElementGrabber::newFrameEventArgs, grabber, &ElementGrabber::updatePixels);
    return grabber;
}

ElementBase * Opto::addMic(ofVec2f p)
{
    ElementMic * mic = new ElementMic(p);
    elements.push_back(mic);
    return mic;
}

ElementBase * Opto::addSolid(ofVec2f p)
{
    ElementSolid * solid = new ElementSolid(p);
    elements.push_back(solid);
    return solid;
}

Sensor * Opto::addSensor(int id, int type, ofVec2f p)
{
    Sensor * sensor = new Sensor(id, type, p);
    sensors.push_back(sensor);
    return sensor;
}
void Opto::onMoved(ofVec2f p){
    if(!GUI::started){
        splash.onMoved(p);
    }
}

void Opto::onPress(ofVec2f p)
{
    if(!GUI::started){
        splash.pressed(p);
    }
    if (!GUI::isOver())
        cDraggable = getDraggable(p);
    if (cDraggable)
    {
        cDraggable->onTouch(p);
    }
    
    int h = 50;
    int xp = 250;
    int yp = ofGetHeight() - h + 5;
    int step = 80;
    
    int selectedElement = -1;
    if (ofGetMouseY() > ofGetHeight() - h)
    {
        selectedElement = (ofGetMouseX() - xp - step) / step;
    }
    ofVec2f np = ofGetCurrentViewport().getCenter();
    if (selectedElement == Globals::ICO_SOLID) addSolid(np);
    if (selectedElement == Globals::ICO_IMAGE)
    {
        ofFileDialogResult fd = ofSystemLoadDialog("", "");
        if (ofToLower(fd.fileName).find("jpg") != std::string::npos || ofToLower(fd.fileName).find("png") != std::string::npos)
        {
            addImage(fd.fileName, np);
        }
    }
    if (selectedElement == Globals::ICO_MOVIE)
    {
        ofFileDialogResult fd = ofSystemLoadDialog("", "");
        if (ofToLower(fd.fileName).find("mov") != std::string::npos)
        {
            addMovie(fd.fileName, np);
        }
    }
    if (selectedElement == Globals::ICO_CAMERA) addGrabber(np);
    if (selectedElement == Globals::ICO_MIC) addMic(np);
}

void Opto::onDrag(ofVec2f p)
{
    
    if (cDraggable)
    {
        cDraggable->onDrag(p);
    }
}

void Opto::onRelease(ofVec2f p)
{
    if (cDraggable)
        cDraggable->onRelease(p);
    cDraggable = NULL;
}

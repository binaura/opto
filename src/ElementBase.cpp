//
//  OptoTexture.cpp
//  Opto
//
//  Created by bence samu on 06/03/15.
//
//

#include "ElementBase.h"

void ElementBase::baseSetup(ofVec2f p)
{
    float s = 200;
    setupView(ofRectangle(p.x - s / 2., p.y - s / 2., s, s));
    useScaler = true;
}

void ElementBase::baseDrawGUI()
{
    ofNoFill();
    ofSetColor(0,97,111);
    ofSetLineWidth(1);
    ofRect(view);
    ofSetLineWidth(1);
    drawScaler();
    drawGUI();
}


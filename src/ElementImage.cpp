//
//  ElementImage.cpp
//  Opto
//
//  Created by bence samu on 08/03/15.
//
//

#include "ElementImage.h"
#include "LetterBoxView.h"

ElementImage::ElementImage(string fileName, ofVec2f p)
{
    ofLoadImage(texture, fileName);
    shift = 0;
    speed = 0;
    rotate = 0;
    baseSetup(p);
    type = OPTO_ELEMENT_IMAGE;
}

void ElementImage::update()
{
    shift += powf(speed, 3) * 0.1;
    shift = fmodf(shift, 1);
    if (shift < 0) shift += 1;
}

void ElementImage::draw()
{
    
    if (rotate > 0)
    {
        ofPushMatrix();
        ofTranslate(view.x + view.width / 2., view.y + view.height / 2.);
        ofRotate(ofMap(shift, 0, 1, 0, 360));
        LetterBoxView::begin(ofRectangle(0, 0, texture.getWidth(), texture.getHeight()), ofRectangle(-view.width / 2., -view.height / 2., view.width, view.height));
        ofSetColor(255);
        texture.draw(0, 0);
        LetterBoxView::end();
        ofPopMatrix();
    }
    else
    {
        float w = texture.getWidth();
        float h = texture.getHeight();
        float x = ofMap(shift, 0, 1, 0, w);
        
        LetterBoxView::begin(ofRectangle(0, 0, texture.getWidth(), texture.getHeight()), view);

        ofMesh mesh;
        mesh.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
        
        mesh.addVertex(ofVec2f(0, 0));
        mesh.addTexCoord(ofVec2f(x, 0));
        
        mesh.addVertex(ofVec2f(0, h));
        mesh.addTexCoord(ofVec2f(x, h));
        
        mesh.addVertex(ofVec2f(w-x, 0));
        mesh.addTexCoord(ofVec2f(w, 0));
        
        mesh.addVertex(ofVec2f(w-x, h));
        mesh.addTexCoord(ofVec2f(w, h));

        mesh.addVertex(ofVec2f(w-x, 0));
        mesh.addTexCoord(ofVec2f(0, 0));
        
        mesh.addVertex(ofVec2f(w-x, h));
        mesh.addTexCoord(ofVec2f(0, h));

        mesh.addVertex(ofVec2f(w, 0));
        mesh.addTexCoord(ofVec2f(x, 0));
        
        mesh.addVertex(ofVec2f(w, h));
        mesh.addTexCoord(ofVec2f(x, h));

        ofSetColor(255);
        texture.bind();
        mesh.draw();
        texture.unbind();
        
        LetterBoxView::end();
    }
}

void ElementImage::drawGUI()
{
    GUI::begin(ofRectangle(view.x, view.y + view.height, view.width , GUI::getGroupHeight(2)), true);
    if (rotate > 0)
    {
        GUI::doToggle(&rotate, "rotate");
        GUI::doSlider(&speed, -1, 1, "rotate");
    }
    else
    {
        GUI::doToggle(&rotate, "rotate");
        GUI::doSlider(&speed, -1, 1, "scroll");
    }
}
